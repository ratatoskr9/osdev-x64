#include <libkrt/krtkiosk.h>
#include <libkrt/kstring.h>

void kiosk_write(krt_kiosk_t *kiosk_inst, const char *str)
{
    if (!kiosk_inst)
        return;
    return kiosk_inst->write(kiosk_inst, str);
}

void kiosk_printf(krt_kiosk_t *kiosk_inst, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    char str[256];
    kvsprintf(str, format, args);
    kiosk_write(kiosk_inst, str);
    va_end(args);
}

char *kiosk_read(krt_kiosk_t *kiosk_inst)
{
    if (!kiosk_inst)
        return NULL;
    return kiosk_inst->read(kiosk_inst);
}
