#include <libkrt/krtmem/bootmem.h>
#include <stdbool.h>

/**
 * @brief This function is used to find a free block of memory in a `bootmem_allocator_t` memory allocator.
 *
 * @param alloc A pointer to a `bootmem_allocator_t` object representing the memory allocator.
 * @param find_first A boolean value indicating whether to search for the first (`true`) or last (`false`) free block.
 *
 * @return The index of the found free block in the memory pool, or `BOOTMEM_NUM_BLOCKS` if no free blocks were found.
 */
size_t __bootmem_find_block(bootmem_allocator_t *alloc, bool find_first);

/**
 * @brief This function is used to find a range of free blocks of memory in a `bootmem_allocator_t` memory allocator that is large enough to fulfill an allocation request.
 *
 * @param alloc A pointer to a `bootmem_allocator_t` object representing the memory allocator.
 * @param blocks_needed The number of blocks needed to fulfill the allocation request.
 *
 * @return The index of the first block in the found range of free blocks, or `BOOTMEM_NUM_BLOCKS - 1` if no suitable range of blocks was found.
 */
size_t __bootmem_find_blocks(bootmem_allocator_t *alloc, size_t blocks_needed);

/**
 * @brief This function is used to get the size of an allocated block of memory from a `bootmem_allocator_t` memory allocator.
 *
 * @param alloc_ptr A pointer to a `krt_allocator_t` object representing the `bootmem_allocator_t` memory allocator.
 * @param ptr A pointer to the start of the allocated memory block.
 *
 * @return The size of the allocated memory block, in bytes.
 */
size_t __bootmem_get_size(krt_allocator_t *alloc_ptr, void *ptr);

/**
 * @brief This function is used to set the size of an allocated block of memory in a `bootmem_allocator_t` memory allocator.
 *
 * @param alloc_ptr A pointer to a `krt_allocator_t` object representing the `bootmem_allocator_t` memory allocator.
 * @param ptr A pointer to the start of the allocated memory block.
 * @param size The size of the allocated memory block, in bytes.
 *
 * @return This function does not return a value.
 */
void __bootmem_set_size(krt_allocator_t *alloc_ptr, void *ptr, size_t size);

/**
 * @brief This function is used to allocate a block of memory from a `bootmem_allocator_t` memory allocator.
 *
 * @param alloc_ptr A pointer to a `krt_allocator_t` object representing the `bootmem_allocator_t` memory allocator.
 * @param size The size of the memory block to allocate, in bytes.
 * @param align The alignment of the allocated memory block, in bytes.
 *
 * @return A pointer to the start of the allocated memory block, or `NULL` if the allocation request could not be fulfilled.
 */
void *__bootmem_malloc(krt_allocator_t *alloc_ptr, size_t size);

/**
 * @brief This function is used to deallocate a block of memory from a `bootmem_allocator_t` memory allocator.
 *
 * @param alloc_ptr A pointer to a `krt_allocator_t` object representing the `bootmem_allocator_t` memory allocator.
 * @param ptr A pointer to the start of the allocated memory block to deallocate.
 *
 * @return The given `ptr` value.
 */
void *__bootmem_dealloc(krt_allocator_t *alloc_ptr, void *ptr);

/**
 * @var bootmem_allocator_t __bootmem
 *
 * @brief The `__bootmem` variable is an instance of the `bootmem_allocator_t` type of memory allocator.
 *
 * @details The `bootmem_allocator_t` type is a memory allocator that uses a fixed-size memory pool and the first-fit
 * algorithm to allocate blocks of memory. The `__bootmem` variable represents an instance of this allocator type, and
 * it contains a memory pool and a bitmap for tracking the usage of blocks in the memory pool, as well as an array of
 * sizes for storing the sizes of allocated blocks. The `__bootmem` variable also implements the functions defined by
 * the `krt_allocator_t` interface for allocating and deallocating memory.
 */
bootmem_allocator_t __bootmem = {
    .super = {
        .get_size = __bootmem_get_size,
        .set_size = __bootmem_set_size,
        .malloc = __bootmem_malloc,
        .dealloc = __bootmem_dealloc,
        .__grow = NULL,
        .__shrink = NULL,
    },
    .bitmap = {0},
    .pool = {0},
    .sizes = {0},
};

krt_allocator_t *bootmem = &__bootmem;

size_t __bootmem_find_block(bootmem_allocator_t *alloc, bool find_first)
{
    // Determine the starting block index for the search based on the
    // 'find_first' input parameter
    size_t block_index = find_first ? 0 : BOOTMEM_NUM_BLOCKS - 1;

    // Search for the first (or last) occurrence of a 0 byte in the 'bitmap'
    // field of the 'bootmem_allocator_t' object, starting at the specified block
    // index and continuing until the end of the 'bitmap' field
    size_t *found = (size_t *)krt_memchr(alloc->bitmap + block_index, 0, BOOTMEM_NUM_BLOCKS - block_index);

    // If a 0 byte was found, return the index of the block corresponding to the 0 byte
    if (found)
        return BOOTMEM_BLOCK_INDEX(found, alloc->bitmap);

    // If no 0 bytes were found, return BOOTMEM_NUM_BLOCKS to indicate that no free blocks were found
    return BOOTMEM_NUM_BLOCKS;
}

size_t __bootmem_find_blocks(bootmem_allocator_t *alloc, size_t blocks_needed)
{
    // Find first free block
    size_t start_block = __bootmem_find_block(alloc, true);

    // Find last free block
    size_t end_block = __bootmem_find_block(alloc, false);

    // Check if the found free block is large enough to fulfill the allocation request
    if (end_block - start_block + 1 >= blocks_needed)
        return start_block;

    // Return the index of the last block if no suitable block was found
    return BOOTMEM_NUM_BLOCKS - 1;
}

size_t __bootmem_get_size(krt_allocator_t *alloc_ptr, void *ptr)
{
    // Get the real allocator from the pointer
    bootmem_allocator_t *alloc = (bootmem_allocator_t *)alloc_ptr;

    // Calculate the index of the block in the memory pool
    size_t index = BOOTMEM_BLOCK_INDEX(ptr, alloc->pool);

    // Return the sizes field of the bootmem_allocator_t object for the given index
    return alloc->sizes[index];
}

void __bootmem_set_size(krt_allocator_t *alloc_ptr, void *ptr, size_t size)
{
    // Get the real allocator from the pointer
    bootmem_allocator_t *alloc = (bootmem_allocator_t *)alloc_ptr;

    // Calculate the index of the block in the memory pool
    size_t index = BOOTMEM_BLOCK_INDEX(ptr, alloc->pool);

    // Set the sizes field of the bootmem_allocator_t object for the given index
    alloc->sizes[index] = size;
}

void *__bootmem_malloc(krt_allocator_t *alloc_ptr, size_t size)
{
    // Get the real allocator from the pointer
    bootmem_allocator_t *alloc = (bootmem_allocator_t *)alloc_ptr;

    // Calculate the number of blocks needed to allocate the given size
    size_t blocks_needed = BOOTMEM_BLOCKS_NEEDED(size);

    // Find a free block with enough space using the helper function
    size_t start_block = __bootmem_find_blocks(alloc, size);

    // Return NULL if no free block was found
    if (start_block == -1)
        return NULL;

    // Set the corresponding bits in the bitmap to 1
    krt_memset(alloc->bitmap + start_block, 1, blocks_needed);

    // Align the pointer to the specified alignment
    void *ptr = (void *)BOOTMEM_BLOCK_POINTER(start_block, alloc->pool);

    // Set the pointer info in the allocator
    krt_setsizeof(alloc, ptr, size);

    // Return the aligned pointer
    return ptr;
}

void *__bootmem_dealloc(krt_allocator_t *alloc_ptr, void *ptr)
{
    // Get the real allocator from the pointer
    bootmem_allocator_t *alloc = (bootmem_allocator_t *)alloc_ptr;

    // Calculate the index of the first block of the memory allocation in the memory pool
    size_t start_block = BOOTMEM_BLOCK_INDEX(ptr, alloc->pool);

    // Calculate the number of blocks needed to allocate the given size
    size_t blocks_needed = BOOTMEM_BLOCKS_NEEDED(krt_sizeof(alloc, ptr));

    // Set the corresponding bits in the bitmap to 0
    krt_memset(alloc->bitmap + start_block, 0, blocks_needed);

    // Return a NULL pointer to indicate that the deallocation succeeded
    return NULL;
}
