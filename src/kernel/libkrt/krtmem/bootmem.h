#ifndef KRT_MEM_BOOTMEM_H
#define KRT_MEM_BOOTMEM_H

#include <libkrt/krtmem.h>

/**
 * BOOTMEM_BLOCK_SIZE - the size of each memory block in bytes
 *
 * This macro defines the size of each memory block in the memory pool in bytes.
 */
#define BOOTMEM_BLOCK_SIZE 16

/**
 * BOOTMEM_NUM_BLOCKS - the number of blocks in the memory pool
 *
 * This macro defines the number of blocks in the memory pool.
 */
#define BOOTMEM_NUM_BLOCKS 1024

/**
 * BOOTMEM_BLOCKS_NEEDED - calculates the number of blocks needed to allocate a given size
 *
 * @size: the number of bytes to allocate
 *
 * This macro calculates the number of blocks needed to allocate a given size by
 * dividing the size by the size of each block, and rounding up to the nearest
 * whole block. The size is aligned to the size of void*.
 *
 * Return: the number of blocks needed to allocate the given size.
 */
#define BOOTMEM_BLOCKS_NEEDED(size) ((ALIGN(size, sizeof(void *)) + BOOTMEM_BLOCK_SIZE - 1) / BOOTMEM_BLOCK_SIZE)

/**
 * BOOTMEM_BLOCK_INDEX - calculates the index of a block in the memory pool given a pointer
 *
 * @ptr: a pointer to the start of the block
 *
 * This macro calculates the index of a block in the memory pool by subtracting
 * the base address of the memory pool from the given pointer, and dividing the
 * result by the size of each block.
 *
 * Return: the index of the block in the memory pool.
 */
#define BOOTMEM_BLOCK_INDEX(ptr, pool) (((size_t)ptr - (size_t)pool) / BOOTMEM_BLOCK_SIZE)

/**
 * BOOTMEM_BLOCK_POINTER - calculates the starting address of a block in the memory pool
 *
 * @start_block: the index of the starting block
 *
 * This macro calculates the starting address of a block in the memory pool by adding
 * the base address of the memory pool to the product of the block index and the size
 * of each block. This can be used to convert a block index obtained from the bitmap
 * into a pointer that can be used to access the block in the memory pool.
 *
 * Return: the starting address of the block in the memory pool.
 */
#define BOOTMEM_BLOCK_POINTER(start_block, pool) (pool + (start_block * BOOTMEM_BLOCK_SIZE))

/**
 * BOOTMEM_POOL_SIZE - the total size of the memory pool in bytes
 *
 * This macro defines the total size of the memory pool in bytes. It is calculated
 * by multiplying the BOOTMEM_BLOCK_SIZE and BOOTMEM_NUM_BLOCKS macros.
 */
#define BOOTMEM_POOL_SIZE (BOOTMEM_BLOCK_SIZE * BOOTMEM_NUM_BLOCKS)

/**
 * @struct bootmem_allocator_t
 *
 * @brief The `bootmem_allocator_t` structure is a type of `krt_allocator_t` that uses the first-fit algorithm
 * for allocating memory.
 *
 * @details This allocator type has a static memory pool represented by an array of bytes called `pool`.
 * The size of the `pool` array is defined by the `BOOTMEM_POOL_SIZE` macro, which is calculated by multiplying
 * the `BOOTMEM_BLOCK_SIZE` and `BOOTMEM_NUM_BLOCKS` macros. The `bootmem_allocator_t` structure also has an array of bits
 * called `bitmap`, which is used to keep track of the usage of each block in the memory pool. The size
 * of the `bitmap` array is the same as the number of blocks in the memory pool, and a value of 0
 * indicates that the corresponding block is free, and a value of 1 indicates that the block is in use.
 *
 * The `bootmem_allocator_t` structure uses the following macros to manipulate the memory pool and the `bitmap`
 * array: `BOOTMEM_BLOCK_SIZE`, `BOOTMEM_NUM_BLOCKS`, `BOOTMEM_BLOCKS_NEEDED`, `BOOTMEM_BLOCK_INDEX`, `BOOTMEM_BLOCK_POINTER`, and `BOOTMEM_POOL_SIZE`.
 */
typedef struct bootmem_allocator
{
    /**
     * super - the base `krt_allocator_t` object
     *
     * This field stores the base `krt_allocator_t` object that provides the interface for the `bootmem_allocator_t`
     * structure. The `bootmem_allocator_t` structure extends the `krt_allocator_t` structure by implementing the
     * methods declared in its interface using the first-fit algorithm.
     */
    krt_allocator_t super;

    /**
     * pool - the static memory pool
     *
     * This array represents the static memory pool used by the first-fit memory allocator.
     * It has a size of BOOTMEM_POOL_SIZE bytes, which is calculated by multiplying the BOOTMEM_BLOCK_SIZE
     * and BOOTMEM_NUM_BLOCKS macros.
     */
    char pool[BOOTMEM_POOL_SIZE];

    /**
     * bitmap - an array of bits representing the usage of each block in the memory pool
     *
     * This array is used to keep track of which blocks in the memory pool are in use and
     * which are free. It has a size of BOOTMEM_NUM_BLOCKS bits, which is the same as the number
     * of blocks in the memory pool. A value of 0 in the array indicates that the block
     * is free, and a value of 1 indicates that the block is in use.
     */
    char bitmap[BOOTMEM_NUM_BLOCKS];

    /**
     * sizes - an array of bits representing the pointer sizes of each block in the memory pool
     *
     * The `sizes` array is an array of `size_t` that stores the size of each memory allocation.
     * This array is updated by the `malloc` and `dealloc` methods of the `bootmem_allocator_t`
     * object to accurately reflect the properties of each memory allocation. If size is 0,
     * that means that either the block is unused or it is part of an allocation that
     * is bigger than BOOTMEM_BLOCK_SIZE.
     */
    size_t sizes[BOOTMEM_NUM_BLOCKS];
} bootmem_allocator_t;

/**
 * @brief A pointer to a krt_allocator_t structure that uses the first-fit algorithm
 * to manage a static memory pool.
 */
krt_allocator_t *bootmem;

#endif /* KRT_MEM_BOOTMEM_H */
