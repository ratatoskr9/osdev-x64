#ifndef KRT_KIOSK_H
#define KRT_KIOSK_H

#include <stddef.h>

/**
 * @struct krt_kiosk_t
 * @brief This structure represents a kiosk.
 *
 * The `krt_kiosk_t` structure contains function pointers that are used to write to and read from the kiosk.
 */
typedef struct krt_kiosk
{
    /**
     * @brief A function pointer that is used to write to the kiosk.
     *
     * This function pointer should point to a function that takes a `krt_kiosk_t` pointer and a `const char *` as arguments and returns void.
     *
     * @param kiosk_inst A pointer to the `krt_kiosk_t` structure.
     * @param str A `const char *` containing the string to be written to the kiosk.
     */
    void (*write)(struct krt_kiosk *kiosk_inst, const char *str);

    /**
     * @brief A function pointer that is used to read from the kiosk.
     *
     * This function pointer should point to a function that takes a `krt_kiosk_t` pointer as an argument and returns a `char *`.
     *
     * @param kiosk_inst A pointer to the `krt_kiosk_t` structure.
     * @returns A `char *` containing the string read from the kiosk.
     */
    char *(*read)(struct krt_kiosk *kiosk_inst);
} krt_kiosk_t;

/**
 * @brief Writes a string to a kiosk.
 *
 * This function writes a given string to a kiosk using the `write` function pointer from the `krt_kiosk_t` structure.
 *
 * @param kiosk_inst A pointer to the `krt_kiosk_t` structure representing the kiosk to be written to.
 * @param str A `const char *` containing the string to be written to the kiosk.
 */
void kiosk_write(krt_kiosk_t *kiosk_inst, const char *str);

/**
 * @brief Writes a formatted string to a kiosk.
 *
 * This function writes a formatted string to a kiosk using the `write` function pointer from the `krt_kiosk_t` structure. The format of the string is specified using a `printf`-style format string, with additional arguments for any format specifiers in the string.
 *
 * @param kiosk_inst A pointer to the `krt_kiosk_t` structure representing the kiosk to be written to.
 * @param format A `const char *` containing the format string.
 * @param ... Additional arguments for any format specifiers in the format string.
 */
void kiosk_printf(krt_kiosk_t *kiosk_inst, const char *format, ...);

/**
 * @brief Reads a string from a kiosk.
 *
 * This function reads a string from a kiosk using the `read` function pointer from the `krt_kiosk_t` structure.
 *
 * @param kiosk_inst A pointer to the `krt_kiosk_t` structure representing the kiosk to be read from.
 * @returns A `char *` containing the string read from the kiosk.
 */
char *kiosk_read(krt_kiosk_t *kiosk_inst);

#endif /* KRT_KIOSK_H */
