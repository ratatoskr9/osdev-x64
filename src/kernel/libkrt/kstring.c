#include <libkrt/kstring.h>

#define abs(n) ((n < 0) ? (-n) : (n))

int isdigit(int c)
{
    return c >= '0' && c <= '9';
}

void swap(char *str1, char *str2)
{
    char tmp = *str1;
    *str1 = *str2;
    *str2 = tmp;
}

char *kstrcpy(char *dest, const char *src)
{
    char *temp = dest;
    while (*dest++ = *src++)
        ;
    return temp;
}

char *kstrrev(char *dest, size_t low, size_t high)
{
    while (low < high)
        swap(&dest[low++], &dest[high--]);
    return dest;
}

char *kstrcat(char *dest, char *str1, char *str2)
{
    kstrcpy(dest, str1);
    kstrcpy(dest + kstrlen(str1), str2);
    return dest;
}

char *kitoa(char *dest, int value, size_t base)
{
    if (base < 2 || base > 32)
        return dest;
    int n = abs(value);
    size_t index = 0;
    for (; n != 0; n /= base)
    {
        int remainder = n % base;
        if (remainder >= 10)
            dest[index++] = 65 + (remainder - 10);
        else
            dest[index++] = 48 + remainder;
    }
    if (index == 0)
        dest[index++] = '0';
    if (value < 0 && base == 10)
        dest[index++] = '-';
    dest[index] = '\0';
    return kstrrev(dest, 0, index - 1);
}

char *kutoa(char *dest, size_t value, size_t base)
{
    if (base < 2 || base > 32)
        return dest;
    size_t n = value;
    size_t index = 0;
    for (; n != 0; n /= base)
    {
        int remainder = n % base;
        if (remainder >= 10)
            dest[index++] = 65 + (remainder - 10);
        else
            dest[index++] = 48 + remainder;
    }
    if (index == 0)
        dest[index++] = '0';
    dest[index] = '\0';
    return kstrrev(dest, 0, index - 1);
}

size_t kstrlen(const char *str)
{
    size_t size = 0;
    while (str && str[size] != '\0')
        size++;
    return size;
}

size_t kstrpad(char *dest, const char *src, char pad_char, size_t width, krt_string_padding_t padding)
{
    switch (padding)
    {
    case PADDING_LEFT:
    {
        return kstrpadl(dest, src, pad_char, width);
    }
    case PADDING_RIGHT:
    {
        return kstrpadr(dest, src, pad_char, width);
    }
    }
}

size_t kstrpadl(char *dest, const char *src, char pad_char, size_t width)
{
    size_t size = 0;
    width = width > kstrlen(src) ? width - kstrlen(src) : 0;
    for (; width > 0; width--)
        dest[size++] = pad_char;
    kstrcpy(dest + size, src);
    size += kstrlen(src);
    return size;
}

size_t kstrpadr(char *dest, const char *src, char pad_char, size_t width)
{
    size_t size = kstrlen(src);
    width = width > kstrlen(src) ? width - kstrlen(src) : 0;
    kstrcpy(dest, src);
    for (; width > 0; width--)
        dest[size++] = pad_char;
    return size;
}

size_t kvsprintf(char *str, const char *format, va_list args)
{
    size_t size = 0;

    // Iterate over the format string
    for (size_t i = 0; format[i] != '\0'; i++)
    {
        if (format[i] != '%')
        {
            // If the character is not a format specifier, just append it to the string
            str[size++] = format[i];
            continue;
        }

        // Parse the format specifier
        char buffer[102];
        size_t width = 0;
        krt_string_padding_t padding = PADDING_LEFT;
        char padding_char = ' ';

        i++;
        if (format[i] == '-')
        {
            // The padding character is the default
            padding = PADDING_RIGHT;
            i++;
        }
        else if (format[i] == '0')
        {
            // The padding character is 0
            padding = PADDING_RIGHT;
            padding_char = '0';
            i++;
        }
        while (isdigit(format[i]))
        {
            // Parse the width
            width = width * 10 + (format[i] - '0');
            i++;
        }

        // Handle the different format specifiers
        switch (format[i])
        {
        case 'd':
        case 'i':
        {
            // Convert an integer argument to a string and append it to the string
            int value = va_arg(args, int);
            kstrpad(buffer, kitoa(buffer, value, 10), padding_char, width, padding);
            kstrcpy(str + size, buffer);
            size += kstrlen(buffer);
            break;
        }
        case 'x':
        {
            // Convert an unsigned integer argument in base 16 to a string and append it to the string
            size_t value = va_arg(args, size_t);
            kstrpad(buffer, kutoa(buffer, value, 16), padding_char, width, padding);
            kstrcpy(str + size, buffer);
            size += kstrlen(buffer);
            break;
        }
        case 'u':
        {
            // Convert an unsigned integer argument in base 10 to a string and append it to the string
            size_t value = va_arg(args, size_t);
            kstrpad(buffer, kutoa(buffer, value, 10), padding_char, width, padding);
            kstrcpy(str + size, buffer);
            size += kstrlen(buffer);
            break;
        }
        case 'c':
        {
            // Append a char argument to the string
            char value = va_arg(args, int);
            str[size++] = value;
            break;
        }
        case 's':
        {
            // Append a string argument to the string
            const char *value = va_arg(args, const char *);
            kstrpad(buffer, value, ' ', width, padding);
            kstrcpy(str + size, buffer);
            size += kstrlen(buffer);
            break;
        }
        case 'p':
        {
            // Convert a pointer argument to a string and append it to the string
            char tmp[100];
            size_t value = va_arg(args, size_t);
            kstrpad(buffer, kutoa(buffer, value, 16), padding_char, width, padding);
            kstrcat(tmp, "0x", buffer);
            kstrcpy(str + size, tmp);
            size += kstrlen(tmp);
            break;
        }
        default:
            for (;;)
                ;
            break;
        }
    }
    // Null-terminate the string and return the length
    str[size] = '\0';
    return size;
}
