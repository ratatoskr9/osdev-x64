#ifndef KRT_MEM_H
#define KRT_MEM_H

#include <stddef.h>

/**
 * @brief This macro aligns a value to the nearest multiple of a specified alignment.
 *
 * @param x The value to align.
 * @param a The alignment to use. This must be a power of 2.
 * @return The aligned value.
 */
#define ALIGN(x, a) ALIGN_MASK(x, (size_t)(a)-1)

/**
 * @brief This macro aligns a value to the nearest multiple of a specified alignment
 * using a mask.
 *
 * @param x The value to align.
 * @param mask The mask to use for alignment. This must be a power of 2 minus 1.
 * @return The aligned value.
 */
#define ALIGN_MASK(x, mask) (((x) + (mask)) & ~(mask))

/**
 * @struct krt_allocator_t
 *
 * @brief The `krt_allocator_t` structure is a type of memory allocator that provides advanced features
 * for allocating, resizing, and freeing memory allocations.
 *
 * @details The interface of this allocator type is designed to be composable, allowing multiple allocator
 * objects to be combined and used together. This allows for a more modular and flexible design of
 * the kernel, as different parts of the kernel can use different allocator objects that are optimized
 * for their specific needs.
 *
 * In addition to improving the design of the kernel, this allocator type also offers improved
 * performance compared to other allocator types. By providing fine-grained control over memory
 * allocations, users of this allocator type can make more efficient use of memory, leading to
 * faster and more responsive applications.
 */
typedef struct krt_allocator
{
    /**
     * @brief This method retrieves the size of a memory pointer.
     * @param alloc The allocator object.
     * @param ptr A pointer to a memory allocation.
     * @return The size of the memory allocation.
     */
    size_t (*get_size)(struct krt_allocator *alloc, void *ptr);

    /**
     * @brief This method sets the size of a memory pointer.
     * @param alloc The allocator object.
     * @param ptr A pointer to a memory allocation.
     * @param size The size of the memory allocation.
     */
    void (*set_size)(struct krt_allocator *alloc, void *ptr, size_t size);

    /**
     * @brief This method allocates memory with the specified size.
     *
     * @param alloc The allocator object.
     * @param size The size of the memory allocation in bytes.
     * @return A pointer to the allocated memory, or NULL if the allocation failed.
     */
    void *(*malloc)(struct krt_allocator *alloc, size_t size);

    /**
     * @brief This method frees a memory allocation.
     *
     * @param alloc The allocator object.
     * @param ptr A pointer to the memory allocation to be freed.
     * @return A NULL if freeing succeeds, or the original pointer.
     */
    void *(*dealloc)(struct krt_allocator *alloc, void *ptr);

    /**
     * @brief This method shrinks a memory allocation to a new size.
     * NOTE: This method is optional and may be NULL.
     *
     * @param alloc The allocator object.
     * @param ptr A pointer to the memory allocation to be shrunk.
     * @param size The new size of the memory allocation in bytes.
     * @return A pointer to the shrunk memory allocation, or NULL if the operation failed.
     */
    void *(*__shrink)(struct krt_allocator *alloc, void *ptr, size_t size);

    /**
     * @brief This method grows a memory allocation to a new size.
     * NOTE: This method is optional and may be NULL.
     *
     * @param alloc The allocator object.
     * @param ptr A pointer to the memory allocation to be grown.
     * @param size The new size of the memory allocation in bytes.
     * @return A pointer to the grown memory allocation, or NULL if the operation failed.
     */
    void *(*__grow)(struct krt_allocator *alloc, void *ptr, size_t size);
} krt_allocator_t;

/**
 * @brief This function sets each byte of a memory region to a specified value.
 *
 * @param ptr A pointer to the memory region to be set.
 * @param data The value to set each byte of the memory region to.
 * @param nbytes The number of bytes in the memory region.
 * @return A pointer to the specified memory region.
 */
void *krt_memset(void *ptr, char data, size_t nbytes);

/**
 * @brief This function copies the contents of one memory region to another.
 *
 * @param dst A pointer to the destination memory region.
 * @param src A pointer to the source memory region.
 * @param nbytes The number of bytes to copy.
 * @return A pointer to the destination memory region.
 */
void *krt_memcpy(void *dst, void *src, size_t nbytes);

/**
 * @brief This function sets a specified number of bytes of a memory region to 0.
 *
 * @param ptr A pointer to the memory region to be cleared.
 * @param nbytes The number of bytes to clear.
 * @return A pointer to the specified memory region.
 */
void *krt_memclr(void *ptr, size_t nbytes);

/**
 * @brief Searches for a byte with a specified value in a memory region.
 *
 * This function searches for the first occurrence of a byte with a specified value
 * in a memory region starting at a specified pointer and with a specified number
 * of bytes.
 *
 * @param ptr A pointer to the starting address of the memory region.
 * @param data The value to search for.
 * @param nbytes The number of bytes in the memory region.
 * @return A pointer to the first occurrence of the specified value, or NULL if the value was not found.
 */
void *krt_memchr(void *ptr, char data, size_t nbytes);

/**
 * @brief This function moves data from one memory location to another and then frees the source memory location.
 *
 * @param alloc The allocator object.
 * @param dst A pointer to the destination memory location.
 * @param src A pointer to the source memory location.
 * @param size The number of bytes to copy.
 * @return A pointer to the freed memory allocation.
 */
void *krt_memcarry(krt_allocator_t *alloc, void *dst, void *src, size_t size);

/**
 * @brief This function sets information about a memory pointer.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to a memory allocation.
 * @param size The new size of the memory allocation, in bytes.
 * @return A void pointer to the updated memory allocation.
 */
void *krt_setsizeof(krt_allocator_t *alloc, void *ptr, size_t size);

/**
 * @brief This function returns the size of a memory allocation.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to a memory allocation.
 * @return The size of the memory allocation, in bytes.
 */
size_t krt_sizeof(krt_allocator_t *alloc, void *ptr);

/**
 * @brief This function allocates memory with the specified size and alignment.
 *
 * @param alloc The allocator object.
 * @param size The size of the memory allocation in bytes.
 * @param align The alignment of the memory allocation in bytes.
 * @return A pointer to the allocated memory, or NULL if the allocation failed.
 */
void *krt_malloc(krt_allocator_t *alloc, size_t size, size_t align);

/**
 * @brief This function allocates and clears a block of memory with the specified number
 * of items, size, and alignment.
 *
 * @param alloc The allocator object.
 * @param nitems The number of items in the memory block.
 * @param size The size of each item in the memory block.
 * @param align The alignment of the memory block in bytes.
 * @return A pointer to the allocated and cleared memory, or NULL if the allocation failed.
 */
void *krt_calloc(krt_allocator_t *alloc, size_t nitems, size_t size, size_t align);

/**
 * @brief This function resizes a memory allocation to a new number of items, size, and alignment.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to the memory allocation to be resized.
 * @param size The size of each item in the memory allocation.
 * @param align The new alignment of the memory allocation in bytes.
 * @return A pointer to the resized memory allocation, or NULL if the operation failed.
 */
void *krt_realloc(krt_allocator_t *alloc, void *ptr, size_t size, size_t align);

/**
 * @brief This method reallocates a memory block and (possibly) clears the memory block.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to the memory block to be reallocated.
 * @param nitems The number of elements in the memory block.
 * @param size The size of each element in the memory block.
 * @param align The alignment of the memory block.
 * @return A pointer to the reallocated memory block, or NULL if the reallocation failed.
 */
void *krt_recalloc(krt_allocator_t *alloc, void *ptr, size_t nitems, size_t size, size_t align);

/**
 * @brief This function frees a memory allocation.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to the memory allocation to be freed.
 * @return A pointer to the freed memory allocation.
 */
void *krt_dealloc(krt_allocator_t *alloc, void *ptr);

#endif /* KRT_MEM_H */
