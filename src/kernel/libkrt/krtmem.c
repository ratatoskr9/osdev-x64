#include <libkrt/krtmem.h>

/**
 * @brief This function shrinks a memory allocation to a new size and alignment.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to the memory allocation to be shrunk.
 * @param size The new size of the memory allocation in bytes.
 * @param align The new alignment of the memory allocation in bytes.
 * @return A pointer to the shrunk memory allocation, or NULL if the operation failed.
 */
void *__krt_shrink(krt_allocator_t *alloc, void *ptr, size_t size, size_t align);

/**
 * @brief This function grows a memory allocation to a new size and alignment.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to the memory allocation to be grown.
 * @param size The new size of the memory allocation in bytes.
 * @param align The new alignment of the memory allocation in bytes.
 * @return A pointer to the grown memory allocation, or NULL if the operation failed.
 */
void *__krt_grow(krt_allocator_t *alloc, void *ptr, size_t size, size_t align);

/**
 * @brief This function resizes a memory allocation to a new size and alignment.
 * Unlike the `krt_realloc` function, the `__krt_resize` function only uses the `__krt_shrink` and
 * `__krt_grow` functions to resize memory allocations.
 *
 * @param alloc The allocator object.
 * @param ptr A pointer to the memory allocation to be resized.
 * @param size The new size of the memory allocation in bytes.
 * @return A pointer to the resized memory allocation, or NULL if the operation failed.
 */
void *__krt_resize(krt_allocator_t *alloc, void *ptr, size_t size, size_t align);

void *krt_memset(void *ptr, char data, size_t nbytes)
{
    char *tmp = (char *)ptr;
    for (size_t i = 0; i < nbytes; i++)
        tmp[i] = data;
    return (void *)ptr;
}

void *krt_memcpy(void *dst, void *src, size_t nbytes)
{
    char *tmp = (char *)dst;
    for (size_t i = 0; i < nbytes; i++)
        tmp[i] = ((char *)src)[i];
    return (void *)dst;
}

void *krt_memclr(void *ptr, size_t nbytes)
{
    return krt_memset(ptr, 0, nbytes);
}

void *krt_memchr(void *ptr, char data, size_t nbytes)
{
    // Cast the input pointer to a char pointer
    char *p = (char *)ptr;

    // Iterate over the memory region starting at the input pointer
    // and with the specified number of bytes
    for (size_t i = 0; i < nbytes; i++)
        // If the current byte is equal to the specified data,
        // return a pointer to the current byte
        if (p[i] == data)
            return (void *)(p + i);

    // If the data was not found, return NULL
    return NULL;
}

void *krt_memcarry(krt_allocator_t *alloc, void *dst, void *src, size_t size)
{
    krt_memcpy(dst, src, size);
    krt_dealloc(alloc, src);
    return dst;
}

void *krt_setsizeof(krt_allocator_t *alloc, void *ptr, size_t size)
{
    alloc->set_size(alloc, ptr, size);
    return ptr;
}

size_t krt_sizeof(krt_allocator_t *alloc, void *ptr)
{
    return alloc->get_size(alloc, ptr);
}

void *krt_malloc(krt_allocator_t *alloc, size_t size, size_t align)
{
    return (void *)ALIGN((size_t)alloc->malloc(alloc, size), align);
}

void *krt_calloc(krt_allocator_t *alloc, size_t nitems, size_t size, size_t align)
{
    return krt_memclr(krt_malloc(alloc, nitems * size, align), nitems * size);
}

void *__krt_shrink(krt_allocator_t *alloc, void *ptr, size_t size, size_t align)
{
    if (!ptr)
        return NULL;

    if (alloc && alloc->__shrink)
        // Use the __shrink method provided by the allocator
        return (void *)ALIGN((size_t)alloc->__shrink(alloc, ptr, size), align);

    // Fallback for allocators that don't support shrinking memory
    return size > krt_sizeof(alloc, ptr) ? NULL : ptr;
}

void *__krt_grow(krt_allocator_t *alloc, void *ptr, size_t size, size_t align)
{
    if (!ptr)
        return NULL;

    if (alloc && alloc->__grow)
        // Use the __grow method provided by the allocator
        return (void *)ALIGN((size_t)alloc->__grow(alloc, ptr, size), align);

    // Fallback for allocators that don't support growing memory
    size_t old_size = krt_sizeof(alloc, ptr);

    if (size < old_size)
        return NULL;

    // The new size is the same as the old size,
    // so just return the original pointer
    if (size == old_size)
        return ptr;

    // Fallback algorithm for growing the memory allocation
    return krt_memcarry(alloc, krt_malloc(alloc, size, align), ptr, old_size);
}

void *__krt_resize(krt_allocator_t *alloc, void *ptr, size_t size, size_t align)
{
    // Check if the pointer is NULL or the new size is 0, in which case we cannot resize the memory allocation
    if (ptr == NULL || size == 0)
        return NULL;

    size_t old_size = krt_sizeof(alloc, ptr);

    // Check if we need to __grow or __shrink the memory allocation
    if (size > old_size)
        return __krt_grow(alloc, ptr, size, align);
    else if (size < old_size)
        return __krt_shrink(alloc, ptr, size, align);
    else
        return ptr;
}

void *krt_realloc(krt_allocator_t *alloc, void *ptr, size_t size, size_t align)
{
    // Check if the pointer is NULL, in which case we can just allocate a new memory allocation
    if (ptr == NULL)
        return krt_malloc(alloc, size, align);

    // Check if the real size is 0, in which case we can just deallocate the memory allocation
    // Otherwise, try to resize the memory allocation and return the result
    return size > 0 ? __krt_resize(alloc, ptr, size, align) : krt_dealloc(alloc, ptr);
}

void *krt_recalloc(krt_allocator_t *alloc, void *ptr, size_t nitems, size_t size, size_t align)
{
    // If the requested size is zero, free the block and return NULL
    if (nitems == 0 || size == 0)
        return krt_dealloc(alloc, ptr);

    // Reallocate the memory block with the new size and alignment
    void *new_ptr = krt_realloc(alloc, ptr, nitems * size, align);

    // If reallocation failed, return NULL
    if (new_ptr == NULL)
        return NULL;

    // If the memory block was moved, clear the memory at the new location
    if (new_ptr != ptr)
        krt_memset(new_ptr, 0, nitems * size);

    // Return the pointer to the reallocated memory block
    return new_ptr;
}

void *krt_dealloc(krt_allocator_t *alloc, void *ptr)
{
    return alloc->dealloc(alloc, ptr);
}
