#ifndef KRT_STRING_H
#define KRT_STRING_H

#include <stddef.h>
#include <stdarg.h>

typedef enum krt_string_padding
{
    PADDING_LEFT,
    PADDING_RIGHT,
} krt_string_padding_t;

char *kstrcpy(char *dest, const char *src);
char *kstrrev(char *dest, size_t low, size_t high);
char *kstrcat(char *dest, char *str1, char *str2);
char *kitoa(char *dest, int value, size_t base);
char *kutoa(char *dest, size_t value, size_t base);
size_t kstrlen(const char *str);
size_t kstrpad(char *dest, const char *src, char pad_char, size_t width, krt_string_padding_t padding);
size_t kstrpadl(char *dest, const char *src, char pad_char, size_t width);
size_t kstrpadr(char *dest, const char *src, char pad_char, size_t width);
size_t kvsprintf(char *str, const char *format, va_list args);

#endif /* KRT_STRING_H */