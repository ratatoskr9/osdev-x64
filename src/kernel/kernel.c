#include <libkrt/krtmem/bootmem.h>
#include <libkrt/kstring.h>
#include <libkrt/krtkiosk.h>
#include <libkrt/krtmem.h>
#include <stdint.h>
#include <stddef.h>
#include <limine.h>
#include <serial.h>

// The Limine requests can be placed anywhere, but it is important that
// the compiler does not optimise them away, so, usually, they should
// be made volatile or equivalent.

static volatile struct limine_terminal_request terminal_request =
    {
        .id = LIMINE_TERMINAL_REQUEST,
        .revision = 0};

static volatile struct limine_memmap_request memmap_request =
    {
        .id = LIMINE_MEMMAP_REQUEST,
        .revision = 0};

static volatile struct limine_smp_request smp_request =
    {
        .id = LIMINE_SMP_REQUEST,
        .revision = 0};

static const char *limine_memmap_entry_text[8] =
    {
        "MEMMAP_USABLE\0",
        "MEMMAP_RESERVED\0",
        "MEMMAP_ACPI_RECLAIMABLE\0",
        "MEMMAP_ACPI_NVS\0",
        "MEMMAP_BAD_MEMORY\0",
        "MEMMAP_BOOTLOADER_RECLAIMABLE\0",
        "MEMMAP_KERNEL_AND_MODULES\0",
        "MEMMAP_FRAMEBUFFER\0"};

static void done(void)
{
    for (;;)
    {
        __asm__("hlt");
    }
}

void print_memmap_entry(krt_kiosk_t *kiosk_inst, struct limine_memmap_entry *entry)
{
    const char *entry_type = limine_memmap_entry_text[entry->type];
    size_t low = entry->base;
    size_t high = low + entry->length;
    kiosk_printf(kiosk_inst, "%p-%p(%04u): %s\n", low, high, high / low, entry_type);
}

void write_to_terminal(krt_kiosk_t *kiosk_inst, const char *str)
{
    struct limine_terminal *terminal = terminal_request.response->terminals[0];
    terminal_request.response->write(terminal, str, kstrlen(str));
}

// The following will be our kernel's entry point.
void _start(void)
{
    // Ensure we got a terminal
    if (terminal_request.response == NULL || terminal_request.response->terminal_count < 1)
        done();

    // Get serial working
    if (!serial_initialize())
        return;

    // We should now be able to call the Limine terminal to print out
    // a simple "Hello World" to screen.
    krt_kiosk_t terminal_kiosk;
    terminal_kiosk.write = write_to_terminal;
    terminal_kiosk.read = NULL;
    kiosk_printf(&terminal_kiosk, "Hello, %s!\n", "world");

    for (uint64_t i = 0; i < memmap_request.response->entry_count; i++)
        print_memmap_entry(&terminal_kiosk, memmap_request.response->entries[i]);
    for (uint64_t i = 0; i < smp_request.response->cpu_count; i++)
        kiosk_write(&terminal_kiosk, "CPU core detected. \n");

    // Testing "first-fit" allocator...
    size_t align = sizeof(void *);

    char *str1 = (char *)krt_malloc(bootmem, 10, align);
    kstrcpy(str1, "Hello");
    kiosk_printf(&terminal_kiosk, "malloc: %s (%p) [%d]\n", str1, str1, krt_sizeof(bootmem, str1));

    str1 = (char *)krt_realloc(bootmem, str1, 20, align);
    kstrcat(str1, str1, ",");
    kiosk_printf(&terminal_kiosk, "realloc: %s (%p) [%d]\n", str1, str1, krt_sizeof(bootmem, str1));

    str1 = (char *)krt_realloc(bootmem, str1, 30, align);
    kstrcat(str1, str1, " world!");
    kiosk_printf(&terminal_kiosk, "realloc: %s (%p) [%d]\n", str1, str1, krt_sizeof(bootmem, str1));

    krt_dealloc(bootmem, str1);

    // We're done, just hang...
    done();
}
