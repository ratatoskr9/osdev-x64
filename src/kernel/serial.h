#ifndef SERIAL_H
#define SERIAL_H

#include <stdbool.h>

#define SERIAL_COM1 0x3f8

int serial_initialize();
bool serial_received();
bool serial_is_transmit_empty();
char serial_recv();
void serial_send(char data);
void serial_write(const char *str);

#endif /* SERIAL_H */