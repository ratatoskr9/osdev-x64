#include <serial.h>
#include <stddef.h>
#include <stdint.h>
#include <libkrt/kstring.h>

uint8_t __serial_inportb(uint16_t port)
{
    unsigned char rv;
    __asm__ volatile("inb %1, %0" : "=a"(rv) : "dN"(port));
    return rv;
}

void __serial_outportb(uint16_t port, uint8_t data)
{
    __asm__ volatile ("outb %1, %0" : : "dN" (port), "a" (data));
}

int serial_initialize()
{
    __serial_outportb(SERIAL_COM1 + 1, 0x00);    // Disable all interrupts
    __serial_outportb(SERIAL_COM1 + 3, 0x80);    // Enable DLAB (set baud rate divisor)
    __serial_outportb(SERIAL_COM1 + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
    __serial_outportb(SERIAL_COM1 + 1, 0x00);    //                  (hi byte)
    __serial_outportb(SERIAL_COM1 + 3, 0x03);    // 8 bits, no parity, one stop bit
    __serial_outportb(SERIAL_COM1 + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
    __serial_outportb(SERIAL_COM1 + 4, 0x0B);    // IRQs enabled, RTS/DSR set
    __serial_outportb(SERIAL_COM1 + 4, 0x1E);    // Set in loopback mode, test the serial chip
    __serial_outportb(SERIAL_COM1 + 0, 0xAE);    // Test serial chip (send byte 0xAE and check if serial returns same byte)

    // Check if serial is faulty (i.e: not same byte as sent)
    if (__serial_inportb(SERIAL_COM1 + 0) != 0xAE)
        return 0;

    // If serial is not faulty set it in normal operation mode
    // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
    __serial_outportb(SERIAL_COM1 + 4, 0x0F);
    return 1;
}

bool serial_received()
{
    return __serial_inportb(SERIAL_COM1 + 5) & 1;
}

bool serial_is_transmit_empty()
{
    return __serial_inportb(SERIAL_COM1 + 5) & 0x20;
}

char serial_recv()
{
    while (!serial_received());
    return __serial_inportb(SERIAL_COM1);
}

void serial_send(char data)
{
    while (!serial_is_transmit_empty());
    return __serial_outportb(SERIAL_COM1, data);
}

void serial_write(const char *str)
{
    for (size_t i = 0; i < kstrlen(str); i++)
        serial_send(str[i]);
}