**osdev** project for learning and exploring os development and design patterns in C. This repo contains a very early ia64 (64-bit) kernel using lumine. The 32-bit version, which is not uploaded anywhere, is much more advanced and almost have a working userspace.

## Screenshots

32-bit kernel

<img src="https://gitlab.com/ratatoskr9/osdev-x64/-/raw/main/images/old_osdev.png" width=75% height=50%>

64-bit kernel

<img src="https://gitlab.com/ratatoskr9/osdev-x64/-/raw/main/images/new_osdev.png" width=75% height=50%>
